FROM ubuntu:20.04

# Install required packages
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
    --no-install-recommends tzdata \
    apache2 mysql-server mysql-client php7.4 libapache2-mod-php7.4 php7.4-mysql php7.4-zip php-mailparse supervisor postfix postfix-pcre composer

# apache config
RUN ln -sf /proc/self/fd/1 /var/log/apache2/access.log && \
    ln -sf /proc/self/fd/1 /var/log/apache2/error.log
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
RUN chown -R www-data:www-data /var/www/

# Install php-mime-mail-parser
RUN mkdir -p /usr/share/safemail
RUN composer require --working-dir=/usr/share/safemail php-mime-mail-parser/php-mime-mail-parser:6.0.0

COPY web/ /var/www/html/
COPY conf/supervisord.conf /etc/supervisor/conf.d/supervisord.conf 
COPY conf/safemail-apache.conf /etc/apache2/sites-enabled/0000-safemail.conf
COPY helperScripts/docker-entrypoint.sh /entrypoint.sh
COPY helperScripts/postfix.sh /opt/
COPY helperScripts/postfixPipe.php /usr/share/safemail/
COPY sql/ /usr/share/safemail/sql/

ENTRYPOINT ["/entrypoint.sh"]
