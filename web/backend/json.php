<?php
/**
 * Safemail
 *
 * PHP version 7.2
 *
 * @param command (takes the whole JSON request)
 *
 * @category Tools
 * @package  SafeMail
 * @author   MindCrew <mc@trshmail.com>
 * @license  GPLV3 gpl.com
 * @link     safemail.itsblue.de
 */

require_once __DIR__ . '/config.php';
require_once __DIR__ . '/logic.php';

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

$safemail = new SafeMail($config);

if (isset($_GET['command'])) {
    $command = json_decode($_GET['command'], true);

    $header = $command['header'];
    $data = $command['data'];

    $response->header = 404;

    switch($header) {
    case 1:
        // login user
        $res = $safemail->loginUser($data['username'], $data['password']);
        if ($res !== "") {
            $response->header = 200;
            $response->data = $res;
        } else {
            $response->header = 401;
        }
        break;

    case 2:
        // login user
        $response->header = $safemail->registerUser($data['username'], $data['password'], $data['email']);

        break;

    case 100:
        // create mail adress
        $response->header = $safemail->createMailAddress($data['session'], $data['domain'], $data['name']);
        break;

    case 200:
        // get all mail addresses
        $res = $safemail->getMailAddresses($data);
        $response->header = $res['header'];
        if ($res['header'] === 200) {
            $response->data = $res['data'];
        }
        break;

    case 201:
        // get all mails
        $res = $safemail->getMails($data['session'], $data['mailAddress']);
        $response->header = $res['header'];
        if ($res['header'] === 200) {
            $response->data = $res['data'];
        }
        break;
    }

    print json_encode($response);
}

?>
