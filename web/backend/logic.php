<?php
/**
 * Safemail
 *
 * PHP version 7.2
 *
 * @category Tools
 * @package  SafeMail
 * @author   MindCrew <mc@trshmail.com>
 * @license  GPLV3 gpl.com
 * @link     safemail.itsblue.de
 */

class SafeMail
{
    private $_config;
    private $_con;
    private $_mailDomain;

    /**
     * Constructor
     *
     * @param mixed $_config config array containing some stuff
     *
     * @return void
     */
    public function __construct($_config)
    {
        $this->_config = $_config;
        $this->_mailDomain = $_config['mailDomain'];
        $this->_con = mysqli_connect($_config['dbhost'], $_config['dbuser'], $_config['dbpassword'], $_config['dbname']);
        if (!$this->_con) {
            echo "<h1>Fatal internal Error! :-/</h1>";
            echo "Error connecting to database: " . mysqli_connect_error();
            http_response_code(500);
            exit();
        }
    }

    /**
     * Function to login users
     *
     * @param string $username username
     * @param string $password passowrd
     *
     * @return string '' or session token
     */
    public function loginUser($username, $password)
    {
        $sql = "SELECT * FROM `users`
        WHERE`username`=\"".$this->_con->real_escape_string($username)."\"";
        $result = $this->_con->query($sql);

        // only one row will be returned
        $data = $result->fetch_assoc();

        if (!password_verify($password, $data['password'])) {
            return "";
        }

        //generate random token
        $length = 10;
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        $token_hash = password_hash($str, PASSWORD_DEFAULT);

        $sql = 'INSERT INTO `sessions` (userId, session)
                VALUES ("'. $data['id'] .'", "'. $token_hash .'")';

        if (!$this->_con->query($sql)) {
            return "";
        }

        return $token_hash;
    }

    /**
     * Function to login users
     *
     * @param string $username username
     * @param string $password passowrd
     * @param string $email    email
     * 
     * @return int (200: success, 406: username or email already exists)
     */
    public function registerUser($username, $password, $email)
    {
        $length = 10;
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        $vercode = password_hash($str, PASSWORD_DEFAULT);

        $sql = "INSERT INTO users (username, password, email, vercode)
        VALUES (\"".$this->_con->real_escape_string($username)."\",
        \"".password_hash($password, PASSWORD_DEFAULT)."\",
        \"".$this->_con->real_escape_string($email)."\", \"$vercode\")";
        $result = $this->_con->query($sql);

        if ($result) {
            return 200;
        } else {
            return 500;
        }


    }


    /**
     * Function to check if a session token exists and get the corresponding user
     *
     * @param string $session session token
     *
     * @return int (-1: does not exist; x>0: userId)
     */
    public function checkSession($session)
    {
        $sql = "SELECT * FROM `sessions`
        WHERE`session`=\"".$this->_con->real_escape_string($session)."\"";
        $result = $this->_con->query($sql);

        if (!$result->num_rows > 0) {
            return -1;
        }

        // only one row will be returned
        $data = $result->fetch_assoc();

        return($data['userId']);
    }

    /**
     * Function to create a new mail address
     *
     * @param string $session session token
     * @param string $domain  the domain the mail is used for
     * @param string $name    the name for the mail
     *
     * @return int http response code
     */
    public function createMailAddress($session, $domain, $name)
    {
        $userId = $this->checkSession($session);

        if ($userId === -1) {
            return 401;
        }

        // generate mail address
        $length = 6;
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }

        $mailAddress = $str."@".$this->_mailDomain;

        $sql = 'INSERT INTO `addresses` (`userId`, `mail`, `domain`, `name`)
        VALUES ("'. $userId .'", "'. $mailAddress .'", "'.$this->_con->real_escape_string($domain).'", "'.$this->_con->real_escape_string($name).'")';

        if (!$this->_con->query($sql)) {
            return 500;
        }

        return 200;

    }

    /**
     * Function to get all mail addresses
     *
     * @param string $session session token
     *
     * @return mixed array with keys 'header' (200 or 401) and 'data' (array with all mail addresses)
     */
    public function getMailAddresses($session)
    {
        $ret = array();
        $ret['header'] = 200;

        $userId = $this->checkSession($session);

        if ($userId === -1) {
            $ret['header'] = 401;
            return $ret;
        }

        $sql = "SELECT * FROM `addresses`
                WHERE userId = $userId";

        $result = $this->_con->query($sql);

        if (!$result->num_rows > 0) {
            return $ret;
        }

        $ret['data'] = array();

        while ($row = $result->fetch_assoc()) {
            array_push($ret['data'], array("id"=>$row['id'], "mailAddress"=>$row["mail"], "domain"=>$row["domain"], "name"=>$row['name'], "active"=>$row['active'] ));
        }

        return $ret;

    }

    /**
     * Function to get all mails
     *
     * @param string $session     session token
     * @param int    $mailAddress id of the mail address
     *
     * @return mixed array with keys 'header' (200 or 401) and 'data' (array with all mails)
     */
    public function getMails($session, $mailAddress)
    {
        $ret = array();
        $ret['header'] = 200;

        $userId = $this->checkSession($session);

        if ($userId === -1) {
            $ret['header'] = 401;
            return $ret;
        }

        $sql = 'SELECT m.id, m.sender, m.subject, m.timestamp, m.body FROM mails m
        INNER JOIN
        addresses a ON (m.addressId = a.id)
        INNER JOIN
        users u ON (a.userId = u.id)

        WHERE
        u.id = '.$userId.' AND
        a.id = '.$this->_con->real_escape_string($mailAddress);

        $result = $this->_con->query($sql);

        if (!$result->num_rows > 0) {
            return $ret;
        }

        $ret['data'] = array();

        while ($row = $result->fetch_assoc()) {
            array_push($ret['data'], $row);
        }

        return $ret;

    }
}

?>
