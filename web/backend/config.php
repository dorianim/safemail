<?php
/**
 * Safemail
 *
 * PHP version 7.2
 *
 * @category Tools
 * @package  SafeMail
 * @author   MindCrew <mc@trshmail.com>
 * @license  GPLV3 gpl.com
 * @link     safemail.itsblue.de
 */

$config['dbhost'] = getenv("DB_HOST");
$config['dbname'] = getenv("DB_NAME");
$config['dbuser'] = getenv("DB_USER");
$config['dbpassword'] = getenv("DB_PASSWORD");
$config['mailDomain'] = getenv("SAFEMAIL_MAILDOMAIN");
?>
