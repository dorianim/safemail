#!/usr/bin/php
<?php

/**
 * Safemail
 *
 * PHP version 7.2
 *
 * @category Tools
 * @package  SafeMail
 * @author   MindCrew <mc@trshmail.com>
 * @license  GPLV3 gpl.com
 * @link     safemail.itsblue.de
 */

require_once '/var/www/html/backend/config.php';
require_once '/usr/share/safemail/vendor/autoload.php';

$con = mysqli_connect($config['dbhost'], $config['dbuser'], $config['dbpassword'], $config['dbname']);
if (!$con) {
    echo "<h1>Fatal internal Error! :-/</h1>";
    echo "Error connecting to database: " . mysqli_connect_error();
    exit();
}

// read from stdin
$fd = fopen("php://stdin", "r");
$emailContent = "";
while (!feof($fd)) {
    $line = fread($fd, 1024);
    $emailContent .= $line;
}
fclose($fd);

$parser = new PhpMimeMailParser\Parser();
$parser->setText($emailContent);
$recievers = $parser->getAddresses('to');

$body = $parser->getMessageBody('html');
if(empty($body)) {
$body = $parser->getMessageBody('text');
}
$subject = $parser->getHeader('subject');
$date = strtotime($parser->getHeader('Date'));
$sender = $parser->getHeader('from');

$recieverIds = array();

for ($i = 0; $i < count($recievers); $i++) {
    $sql = "SELECT * FROM `addresses`
    WHERE`mail`=\"" . $con->real_escape_string($recievers[$i]["address"]) . "\"";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($recieverIds, $row["id"]);
        }
    }
}

for ($i = 0; $i < count($recieverIds); $i++) {
    $sql = 'INSERT INTO mails (sender, addressId, subject, timestamp, body)
            VALUES("' . $con->real_escape_string($sender) . '", '.$recieverIds[$i].', "'.$con->real_escape_string($subject).'", "'.$con->real_escape_string($date).'", "'.$con->real_escape_string($body).'")';
            //echo $sql;
    if (!$con->query($sql)) {
        die("error inserting");
    }
        }
?>
