#!/bin/sh
set -e

if [ $# -eq 0 ]; then
	# mysql commands with passwords and options
	DB_PORT=3306
	myclidbroot="mysql -uroot -P$DB_PORT -h$DB_HOST -p$MYSQL_ROOT_PASSWORD"
	myclidbuser="mysql -u$DB_USER -P$DB_PORT -h$DB_HOST -p$DB_PASSWORD"
	myclisqlshow="mysqlshow -uroot -P$DB_PORT -h$DB_HOST -p$MYSQL_ROOT_PASSWORD"

	# Wait until db host gets available
	dbavailable=0
	echo "Waiting for the database server to come online"
	while [ $dbavailable -eq 0 ]; do
		sleep 2
		$myclidbroot -e "show processlist;" > /dev/null 2>&1 && dbavailable=1
	done

	# Check if DB exists:
	dbexists=0
	$myclisqlshow | sed '1,/Databases/d' | awk '{print $2}' | grep $DB_NAME > /dev/null 2>&1 && dbexists=1


	if [ $dbexists -eq 0 ]; then
		  echo "Database $DB_NAME does not exist."
		  echo "Creating and initializing."
		  $myclidbroot -e "CREATE DATABASE ${DB_NAME};"
		  $myclidbroot -e "CREATE USER IF NOT EXISTS ${DB_USER}@'%' IDENTIFIED BY '${DB_PASSWORD}';"
		  $myclidbroot -e "ALTER USER ${DB_USER}@'%' IDENTIFIED BY '${DB_PASSWORD}';"
		  $myclidbroot -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO ${DB_USER}@'%';"
		  $myclidbroot -e "FLUSH PRIVILEGES;"
		  # Create initial database structure
		  $myclidbuser ${DB_NAME} < /usr/share/safemail/sql/initial-structure.sql
 	 else
		  echo "Database $DB_NAME exists."
		  echo "Setting password as specified in docker-compose.yml."
		  $myclidbroot -e "CREATE USER IF NOT EXISTS ${DB_USER}@'%' IDENTIFIED BY '${DB_PASSWORD}';"
		  $myclidbroot -e "ALTER USER ${DB_USER}@'%' IDENTIFIED BY '${DB_PASSWORD}';"
		  $myclidbroot -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO ${DB_USER}@'%';"
		  $myclidbroot -e "FLUSH PRIVILEGES;"
  	fi

	# Start supervisord
	/usr/bin/supervisord
else
	# Execute given command
	exec "$@"
fi
