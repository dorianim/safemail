# //  SAFE  ||  E-MAIL  //

This is Safemail (good stuff) to manage your online accounts email addresses.

## Information
It's a httf project.

## Installation

#### Port forwading and mx record

- You need an mx record for your domain pointing to your server.
- If you are behind a firewall, you need to forward port 25 to your safemail server.

#### Docker

1. Create a folder for installation:
```bash
mkdir /opt/safemail && cd /opt/safemail
```
2. Clone safemail:
```bash
git clone https://gitlab.com/dorianim/safemail.git safemail-src
```
3. Create a docker-compose.yml with this content:
```yaml
version: "3.7"
services:
  safemail:
    build:
      dockerfile: Dockerfile
      context: safemail-src
    restart: always
    environment:
      SAFEMAIL_MAILDOMAIN: "safemail.example.com"
      DB_HOST: mysql-server
      DB_USER: safemail
      DB_PASSWORD: safemail
      DB_NAME: safemail
      MYSQL_ROOT_PASSWORD: safemailRoot
    depends_on:
      - mysql-server
    ports:
      - "5080:80"
      - "25:25"

  mysql-server:
    image: mysql:5.7
    restart: always
    command: --default-authentication-plugin=mysql_native_password
    environment:
      MYSQL_ROOT_PASSWORD: safemailRoot
    volumes:
      - "./mysql-data:/var/lib/mysql"
```
- Plase change `SAFEMAIL_MAILDOMAIN` to your domain.
- You can also change the `DB_PASSWORD` and `MYSQL_ROOT_PASSWORD` if you want.

4. Start safemail:
```
docker-compose up -d
```

5. Done! You can reach your safemail on `localhost:5080`
6. [OPTIONAL] You may test email delivery like this:
  - Create a file with an email (`email.txt`, change the To address to yours): 
  ```
  From: John Smith <test@example.com>
  To: Joe Smith <JHADw@safemail.example.com>
  Subject: an example.com example email
  Date: Mon, 7 Nov 2016 08:45:16

  Dear Joe,
  Welcome to this example email. What a lovely day.
  ```
  - Send the mail
  `curl smtp://localhost:25 --mail-from test@example.com --mail-rcpt ZDB7ta@safemail.example.com --upload-file email.txt`
7. [OPTIONAL] To setup ssl/https, please use a reverse proxy like nginx

## How To Use

1. Go to your safemail (per default on `localhost:5080`)
2. Register a new account
3. Create a new email address
4. Send an email to this address
5. Open the inbox and see if it arrived 

## Built With

* [PHP](https://php.net//) - Programming language for backend (oh no)
* [HTML](https://www.w3schools.com/html/) - foof
* [CSS](https://www.w3.org/Style/CSS/) - webshit pretty normal
* [Bootstrap](https://getbootstrap.com/) - css stuff y know
* [Docker](https://docker.io) - Container ur stuff
* [Postfix](http://www.postfix.org/) - Mailserver

## Authors

* **Sascha S.** - [Mv0sKff](https://gitlab.com/Mv0sKff)
* **Maris B.** - [TheProgramming_M](https://gitlab.com/TheProgramming_M)
* **Dorian Z.** - [dorianim](https://gitlab.com/dorianim)
* **Max G.** - [SomeThink](https://gitlab.com/SomeThink)
* **Silas S.** - [SILAS.ICH.SCHWOER](https://gitlab.com/SILAS.ICH.SCHWOER)

## License

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
